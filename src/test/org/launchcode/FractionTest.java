package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {

    /*
    Test Constructor -1
    Test Add -5
    Test to string -3
    test equals -2
    test compareTo -4
     */
    @Test
    public void FractionTestMarksTest(){
        Fraction half = new Fraction(1,2);
        assertEquals(1, (int) half.getNumerator());
        assertEquals(2, (int) half.getDenominator());
    }

    @Test
    public void FractionTestEqualsTest(){
        Fraction half = new Fraction(1,2);
        Fraction quarter = new Fraction(1,4);
        assertFalse(half.equals(quarter));
    }
    @Test
    public void FractionTestString() {
        Fraction half = new Fraction(1, 2);
        assertEquals("1/2", half.toString());
    }
   @Test
    public void FractionTestCompareTo(){
        Fraction half = new Fraction(1,2);
        Fraction quarter = new Fraction(1,4);
        assertEquals(-2, half.compareTo(quarter));
    }
    @Test
    public void FractionTestAdd(){
        Fraction half = new Fraction(1,2);
        Fraction quarter = new Fraction(1,4);
        assertEquals("3/4", half.add(quarter));

    }
}
