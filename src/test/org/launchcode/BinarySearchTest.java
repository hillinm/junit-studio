package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.*;

public class BinarySearchTest {
    @Test
    public void BinarySearchMarksTest() {
        int[] numbers = {1, 2, 3, 4};
        assertEquals(0, BinarySearch.binarySearch(numbers, 1));
    }
    @Test
    public void BinarySearchMarksTest1() {
        int[] numbers = {1, 2, 3, 4};
        assertEquals(-1, BinarySearch.binarySearch(numbers, 0));
    }
    @Test
    public void BinarySearchMarksTest2() {
        int[] numbers = {1, 2, 3, 4};
        assertEquals(3, BinarySearch.binarySearch(numbers, 4));
    }
    @Test
    public void BinarySearchMarksTest3() {
        int[] numbers = {1, 2, 3, 4};
        assertEquals(-1, BinarySearch.binarySearch(numbers,44));
    }
}
