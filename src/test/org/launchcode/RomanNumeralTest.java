package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.*;

public class RomanNumeralTest {
    @Test
    public void RomanNumeralTestMarksTest(){
        assertEquals("VII", RomanNumeral.fromInt(7));
    }
    @Test
    public void RomanNumeralTestMarksTest1(){
        assertEquals("III", RomanNumeral.fromInt(3));
    }
    @Test
    public void RomanNumeralTestMarksTest2(){
        assertEquals("IV", RomanNumeral.fromInt(4));
    }
    @Test
    public void RomanNumeralTestMarksTest3(){
        assertEquals("XVIII", RomanNumeral.fromInt(18));
    }
    @Test
    public void RomanNumeralTestMarksTest4()
    {
        assertEquals("XIX", RomanNumeral.fromInt(19));
    }
}
