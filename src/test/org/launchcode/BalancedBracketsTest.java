package org.launchcode;

import org.junit.Test;

import static org.junit.Assert.*;


public class BalancedBracketsTest {

    @Test
    public void hasBalancedBracketsOpenTextClosed(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[LaunchCode]"));
    }
    @Test
    public void hasBalancedBracketsTextOpenTextClosed(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("Launch[Code]"));
    }
    @Test
    public void hasBalancedBracketsOpenClosedText(){
        assertTrue(BalancedBrackets.hasBalancedBrackets("[]LaunchCode"));
    }
    @Test
    public void hasBalancedBracketsNone() {
        assertTrue(BalancedBrackets.hasBalancedBrackets(""));
    }
    @Test
    public void hasBalancedBracketsOpenText(){
             assertFalse(BalancedBrackets.hasBalancedBrackets("[LaunchCode"));
    }
     @Test
    public void hasBalancedBracketsTextClosedTextOpen(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("Launch]Code["));
    }
    @Test
    public void hasBalancedBracketsOpen(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("["));
    }
    @Test
    public void hasBalancedBracketsClosedOpen(){
        assertFalse(BalancedBrackets.hasBalancedBrackets("]["));

    }
}
